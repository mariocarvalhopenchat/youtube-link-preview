//
//  ViewController.swift
//  YoutubeLinkShare
//
//  Created by Mário Carvalho on 05/05/17.
//  Copyright © 2017 Mário Carvalho. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!

    static let cellIdentifier = "YoutubePlayerTableViewCellIdentifier"
    
    let moviesList = ["https://www.youtube.com/watch?v=wpa4GZadnGA",
                      "https://www.youtube.com/watch?v=o8uN6m_YgoA",
                      "https://www.youtube.com/watch?v=4PMk-irLd1w",
                      "https://www.youtube.com/watch?v=4bK5AoU4O3o"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.tableView.register(UINib.init(nibName: "YoutubePlayerTableViewCell", bundle: nil), forCellReuseIdentifier: YoutubePlayerTableViewCell.cellIdentifier)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK - UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: YoutubePlayerTableViewCell.cellIdentifier) as! YoutubePlayerTableViewCell
        
        cell.playerView.loadVideoURL(URL.init(string: moviesList[indexPath.row])!)
        
        return cell
    }

}

