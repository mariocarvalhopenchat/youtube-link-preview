//
//  YoutubePlayerTableViewCell.swift
//  YoutubeLinkShare
//
//  Created by Mário Carvalho on 05/05/17.
//  Copyright © 2017 Mário Carvalho. All rights reserved.
//

import UIKit
import YouTubePlayer

class YoutubePlayerTableViewCell: UITableViewCell {

    static let cellIdentifier = "YoutubePlayerTableViewCellIdentifier"

    @IBOutlet weak var playerView: YouTubePlayerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
